#!/bin/sh

shopt -s globstar
WATCHSOURCE="**/*.rst"
MAKEFUNC=makeindex

function makeindex() {
    make clean
    make html
}


while true ; do
    inotifywait -e modify $WATCHSOURCE -qq
    $MAKEFUNC
done

