Access and Login
================

To gain access your account needs to be added to the **hpc-users** access group.
Drop us an E-Mail if you would like to join, there are no restrictions, we
simply would like to talk to you beforehand.


SSH Terminal
------------

Access to the Tardis is provided via secure shell within the MPIB (or VPN)
network. On macOS and Linux machines the :program:`ssh` program should be
already installed. On more recent windows machines there should be `ssh.exe`,
but we recommend to use WSL2 for a full bash environmet.

To get an instant shell on Tardis run:
::

   ssh <YOUR_USERNAME>@tardis.mpib-berlin.mpg.de

For a while now we also have a `web shell <https://tardis.mpib-berlin.mpg.de/>`_
for when there is no SSH client available.

On the first connection you will be prompted to accept a host fingerprint.
This is to authenticate the host and to prevent so-called machine in the middle
attacks on your connection. The fingerprint should be either one of the
following:

+------------+----------+-------------------------------------------------+
| **Cipher** | **Algo** | **Fingerprint**                                 |
+------------+----------+-------------------------------------------------+
| RSA        | MD5      | b3:c5:6a:3a:e1:cf:ca:38:57:43:19:1e:fc:45:eb:f4 |
+------------+----------+-------------------------------------------------+
| RSA        | SHA-256  | EwUlbWfFa27S9IickmNrEjbKp8yYafJ4+ga+fl4MDU0=    |
+------------+----------+-------------------------------------------------+
| ED25519    | MD5      | 46:5a:90:b1:06:b8:d4:96:24:cc:f5:f5:55:bf:f1:6d |
+------------+----------+-------------------------------------------------+
| ED25519    | SHA-256  | kvAQatFgyA0DidMvKMf7xmGJWehZ3ASR9E0D+nxwFd0=    |
+------------+----------+-------------------------------------------------+

SSH File Transfer
-----------------

There are a number of (graphical) clients to help you transfer and mange files
and directories on Tardis. For the most common operating systems we recommend
the following but you are of course free to use whatever you want.


Linux
^^^^^

Most modern Linux Desktops come with SSH already built-in to their file
manager. In Ubuntu you can simply use ``sftp://`` or ``ssh://`` as a protocol
in Nautilus:

.. image:: ../img/nautilus.png
   :width: 80%

Open the following URL in your file manager (CTRL-L) ::

   ssh://YOUR_USERNAME@tardis.mpib-berlin.mpg.de/home/mpib/

macOS
^^^^^

Apple has unfortunately not implemented the ``ssh://`` protocol family into
their finder and we need to rely on third party tools to integrate the protocol
nicely.

Currently, IT/helpdesk supports the use of a versatile commercial software
called `Mountain Duck`_ You can download a licensed version from the MPIB App
Store (please ask helpdesk if you have issues).


To connect to Tardis with Mountain Duck, click on the duck icon in the macOS top bar (this has been confusing for lot's of users) and add a new connection with the following details:

.. image:: ../img/mduck_01.png
   :width: 70%

The software differs from the classical concept of a "mount" where changes can be inherently asynchronous, resulting in eventual consistency of your data. If this is something you would like to avoid, we recommend you use the following sync settings:

.. image:: ../img/mduck_02.png
   :width: 70%

To connect Mountain Duck to Tardis, you should be able to open the connection and subsequently see your home directory and your data in your finder.

.. image:: ../img/mduck_03.png
   :width: 40%

.. image:: ../img/mduck_04.png
   :width: 40%

macOS (FUSE)
^^^^^^^^^^^^

 Previously we recommend to use the `macfuse`_ project, possibly but not necessarily in combination with `macfusion`_:

.. image:: ../img/osxfuse.png
   :width: 80%

Since macOS 10.15 the stability and ease of installation has been kind of
rough. It still works, but you need to grant special permissions to the OS and
be prepared for the occasional system hang.


Installation
   If you're using an MPIB Mac with Managed Software Center, just install the macfuse or macfusion (gui frontend) package from there:


.. image:: ../img/macfusion_munki.png
   :width: 80%


Otherwise
   1. Download and install `macfuse`_ and make sure to tick the "Compatibility Layer for MacFuse" option
   2. Reboot your machine
   3. Either use ``sshfs`` in the command line or download and install `macfusion`_:

Macfusion Configuration
   1. add a new profile with the hostname ``tardis.mpib-berlin.mpg.de`` and your username
   2. in the Tab **SSH Advanced** enable the option :samp:`Defer Permissions`
   3. starting with macOS High Sierra you need to pass a valid mount point below your user home, for example ``/Users/<you_username>/Volumes/tardis`` - (**Careful**: for some reason Macfusion expects the parent directory to exist, but not the last level. So make sure to create ``/Users/<your_username>/Volumes`` if you chose to use this location.)
   4. run ``ssh <you_username>@tardis.mpib-berlin.mpg.de`` in the terminal and approve the host key once, see above for fingerprints
   5. mount the new profile by clicking on mount
   6. open in finder either by looking for it or click the cogs icon and then *Reveal*
   7. A general remark: this software is really buggy, try removing and re-adding the profile when there are unexplainable errors or let us know
      (Log files are helpful to isolate the problem, they can be found under *cogs*-symbol -> Log)

Using sshfs directly
   Alternatively, you can simply run sshfs in the command line to mount the remote side:

.. code-block:: text

   lip-osx-001106:~ krause$ mkdir -p /Users/krause/Volumes/tardis  # run this only once
   lip-osx-001106:~ krause$ sshfs krause@tardis.mpib-berlin.mpg.de: /Users/krause/Volumes/tardis/
   warning: ssh nodelay workaround disabled
   krause@tardis.mpib-berlin.mpg.de's password: *******
   lip-osx-001106:~ krause$



Windows
^^^^^^^
The most common utility to transfer files on Windows is `WinSCP <https://winscp.net/eng/download.php>`_:

.. image:: ../img/winscp.png
   :width: 80%


Mounting File Servers
---------------------

You can transfer files directly from the tardis to our file servers, without
taking the detour through your client. There are multiple ways to *mount* the
servers.

.. warning::

    When you work on network folders on the tardis there is no such thing as trash.
    When you accidentally remove a folder, it is **gone**. This also accounts for
    mounted network folders! Please remember that restoring large network file server
    backups are extremely costly and take a lot of time.  Also note that the Tardis
    itself (i.e. your Home Directory) does **not** have a backup at all.

udevil
^^^^^^

Udevil is a helper program to manually mount specific folders to your home
directory and it works like this:

Create a mount location (once):

.. code-block:: bash

    krause@login:~> $ mkdir NetworkFolders

Now mount a folder with this command (FB-LIP in this example):

.. code-block:: bash

    krause@login:~> $ udevil mount -t cifs smb://$USER@mpib-berlin.mpg.de/FB-LIP NetworkFolders/

It will ask you for your password:

.. code-block:: bash

    krause@login:~> $ udevil mount -t cifs smb://krause@mpib-berlin.mpg.de/fb-lip NetworkFolders/
    Password:
    Mounted //mpib-berlin.mpg.de/fb-lip at /home/mpib/krause/NetworkFolders

    krause@login:~> $ ls NetworkFolders/ConMEM
    BEH  EEG  MRI  Neurodaten  Project  STUDIES


If you want to mount another folder either create a different target directory or **unmount** the old folder first:

.. code-block:: bash

    krause@login:~> $ udevil umount NetworkFolders/
    krause@login:~> $

azure files
^^^^^^^^^^^

Mounting Azure Files is no different to mounting network shares, you just have to use the username and password that IT provided for your project:

.. code-block:: bash

    krause@login:~> $ udevil mount -t cifs smb://user@user.file.core.windows.net/project NetworkFolders/project


You can check all your currently mounted folders with :program:`mount` and filtering by
your username with :program:`mount | grep $USER`.

.. note::

   Mounting network folders is a manual process and the connection **only exists
   on the machine you issue the mount on**. Primarily for performance reasons,
   they are not transparently handed down to the execution nodes and thus do
   not work in your scripts out of the box. Usually you need to make a copy of
   your data on the login and then have your jobs work on that copy.


.. _macfuse: https://osxfuse.github.io/
.. _macfusion: http://macfusionapp.org/
.. _Mountain Duck: https://mountainduck.io/
