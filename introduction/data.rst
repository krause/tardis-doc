/data
-----

Starting in 2020 we restructured important data directories on the tardis. There are now the following folders:


.. code-block:: none

   /data/
   ├── bids
   │   ├── EnvNeuro
   │   └── Plasticity
   ├── container
   │   ├── bids
   │   └── ...
   ├── home
   │   ├── user01
   │   ├── user02
   │   └── ...
   ├── software
   │   ├── archive
   │   └── live
   └── system

BIDS
~~~~

The BIDS folder will be the new central location for **BIDS-validated**
datasets. Its primary use is to collect (automatically generated) rawdata from
the scanner and (stable, finalized) derivatives. The data will be accesible
within a research group only and it will be read-only. If you want to add
a dataset here, let us know.

Example:

.. code-block:: none

   /data/bids/                 # root
   └── Plasticity              # Group-Level
       └── Aktiv               # Study-Level
           ├── derivatives     # BIDS-level
           ├── rawdata
           └── sourcedata


Container
~~~~~~~~~

The folder :file:`/data/container/` will host :doc:`../environment/singularity` containers for
everyone to use. There are some versioned, automatically built and fetched
containers based on specification file :file:`/data/container/.build.yaml`.
This file is group writable, so feel free to add a container if you think it
would be useful or just let me know and I can add a container for you.
Unofficial, custom built containers can be collected in :file:`/data/container/unofficial/`.

This is an (incomplete) list of the containers available right now:

.. code-block:: none

   /data/container/
   ├── bids
   │   └── validator-v1.4.4.sif
   ├── fmriprep
   │   ├── ...
   │   ├── fmriprep-20.0.3.sif
   │   └── fmriprep-20.0.5.sif
   ├── heudiconv
   │   ├── ...
   │   ├── heudiconv-0.6.0.sif
   │   └── heudiconv-0.7.0.sif
   ├── mriqc
   │   └── mriqc-0.15.1.sif
   ├── qsiprep
   │   ├── ...
   │   └── qsiprep-0.8.0.sif
   └── unofficial
       ├── fsl
       │   ├── fsl-5.0.11.sif
       │   └── fsl-6.0.3.sif
       ├── jags
       │   ├── jags-4.2.0.sif
       │   └── jags-4.3.0.sif
       ├── mrtrix3
       │   ├── mrtrix3-3.0.rc2.sif
       │   └── mrtrix3-3.0.rc3.sif
       └── OpenMx
           └── OpenMx-jessie.simg


User homes
~~~~~~~~~~

The new canonical location for user homes is :file:`/data/home/`. All previous
locations such as :file:`/home/mpib` or :file:`/home/beegfs` are now linking to
that folder and can still be used in your scripts. The storage is provided by
a small cluster of BeeGFS servers and there **is no backup**.
Data will be transparently compressed, so there is no need to zip files
excessively, as long as they stay in your homes.


Software
~~~~~~~~

Regular software, for example accesible through the `module system
<../environment/modules>`_, is stored in :file:`/data/software/`. Often used
software is regularly synchronized to the execution nodes (:file:`live/`),
while older software in the :file:`archive/` folder is available over the
network and can thus be a little slower when loading. If you need some other
software to add, we can always prepare that for you. Just get in touch.
