Rules
=====

There Is No Backup
------------------

.. warning::

    The cluster file systems are redundant (controller and disks) but there is no
    and there never will be a backup of the file system.  Users are responsible for
    copying valuable results and code to the network file servers.

Disk Usage
----------

Please realize that although processing resources are handled automatically
**File and Directory** resources are not. The large, virtual disk is shared
among all users and needs to be treated that way. In particular this means:

+ **Clean Up** on a regular basis
+ **Always Optimize** your I/O operations in your code


Bugs
----

If you find a bug or misconfiguration, please be **reponsible** and disclose it to us. Thanks.
