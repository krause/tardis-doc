
Useful \*nix programs
=====================

This purpose of this section is to list and showcase some useful command line
tools to help with you daily workflow. If you have suggestions for awesome
tools let me know!


.. _tmux:

tmux
----

Tmux is a terminal multiplexer and a replacement for the :file:`screen`
old timer. Think of it as a container for terminals that you can group together.
What makes tmux/screen super useful is the ability to send the group of
terminals "to the background" and attach them at a later point in time.  This
works across different SSH sessions, even simultaneously! 

Here is an example sessions. We start tmux. Inside it we create two terminals
and switch them.  The container can be detached and then re-attached at some
later point:

.. raw:: html
   
   <script src="https://asciinema.org/a/153457.js" id="asciicast-153457" async></script>

Once you started tmux there are a lot of things you can do with it. Each action is prefix with a custom shortcut. This defaults to <Ctrl>-<b>. To activate an action you combine the prefix with the action shortcut. For example, to create a new terminal inside tmux you would hit <Ctrl>-<b>, release the keys and the quickly after, enter a single <c> for **create**. Here is a list of the most important actions:

+ <c> : **c**\ reate a new terminal
+ <n> : switch to the **n**\ ext terminal (increasing index)
+ <p> : switch to the **p**\ revious terminal
+ <d> : **d**\ etach tmux

Other useful actions:

+ <N> : with N being a number between 0 and 9: jump to the terminal with index N
+ <k> : **k**\ ill the current terminal, when a simple exit doesn't work anymore
+ <?> : print the help screen


To "resurrect" an old tmux sessions, just run :program:`tmux a`.
