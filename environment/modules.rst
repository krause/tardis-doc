Modules
=======

Environment modules are a well established means of switching software versions
in the HPC world. The base operating system usually provides the most stable
and secure updates for specific software such as R or python. However, with
most of the research software out there, it's sometimes necessary to use
a specific major version of the software. For example, if some script,
toolbox, or library depends on it.

If you need a software that differs from the operating system base you can let
us know and we try to build and deploy that software for you through the module
system. Notable examples are Matlab and R.

Quick Start
-----------

**List available** modules with

.. code-block:: shell

    module avail

**Activate** a module with an optional version string:

.. code-block:: shell

   module load R[/3.6]


Details
-------

On every Linux there are two environment variables that determine the software
reachable to you: `PATH` and `LD_LIBRARY_PATH`.
In most cases environment modules are simply modifying these two variables. For
instance, if your `PATH` contains `/opt/software/matlab/R2017b/bin` then
everything available in that folder will be available in your shell and when
you run :program:`matlab` it will probably be the one found in there:

.. code-block:: shell

   [krause@ood-43 ~] which matlab
   /usr/local/bin/matlab
   [krause@ood-43 ~] echo $PATH
   /home/mpib/krause/bin:/home/mpib/krause/.local/bin:/usr/bin:/usr/sbin:/opt/bin:/usr/local/bin:/usr/bin:/bin
   [krause@ood-43 ~] module load matlab
   [krause@ood-43 ~] which matlab
   /opt/matlab/R2017b/bin//matlab
   [krause@ood-43 ~] echo $PATH
   /opt/matlab/R2017b/bin/:/home/mpib/krause/bin:/home/mpib/krause/.local/bin:/usr/bin:/usr/sbin:/opt/bin:/usr/local/bin:/usr/bin:/bin


The source definition for an environment file is pretty straight forward, you can have a look in :program:`/opt/environment/modules` to see what they do.

It's possible to conflict with other modules. For example, if you were to load :program:`mrtrix3` and :program:`mrtrix3tissue` you would get an error, because only one implementation of mrtrix3 should be active:


.. code-block:: shell

   [krause@login ~] module load mrtrix3
   [krause@login ~] module load mrtrix3tissue
   mrtrix3tissue/5.2.8(13):ERROR:150: Module 'mrtrix3tissue/5.2.8' conflicts with the currently loaded module(s) 'mrtrix3/rc3'
   mrtrix3tissue/5.2.8(13):ERROR:102: Tcl command execution failed: conflict        "mrtrix3"


To deactivate a module, simply end the shell (a module activation is not persistent) or run :program:`module unload mrtrix3`.

To list all currently active modules, you can use :program:`module list`.

Caveats
-------

Software built in :file:`/opt/software` is prone to break after operating
system upgrades. In cases where the software is compiled statically (R) or
where the software ships their own shared libraries (Matlab), everything will
continue to work. In same cases where software is delivered as a tarball
(freesurfer) it's entirely possible that some internal program will stop
working at some point. This is an inherent problem of shipping dynamically
built software outside of the operating system's own package manager and
nothing we can fix (see the section for Singularity for a different approach to
solve this problem).
