ANTs
====

We used to track ANTs from the `Neurodebian`_ repository, but right now the
package is unavailable and there are some custom compiled environment modules
instead.
To use ANTs, you have to add the installation directory to your environment. This can be
done done with environment modules:


.. code-block:: bash

   module avail ants
   --- /opt/environment/modules ---
   ants/2.1.0-mpib0  ants/2.3.3-mpib0  ants/2.3.5-mpib0

   module load ants

Afterwards all ants* related programs are in your PATH and ready to use.

Some of the convenient template building scripts distribute their tasks with
different cluster systems using the ``-c`` switch. To use SLURM you need to
pass ``-c 5`` to these scripts. For example to build a template with
``antsMultivariateTemplateConstruction2.sh`` you could run:

.. code-block:: bash

   module load ants
   antsMultivariateTemplateConstruction2.sh -d 3 -i 3 -k 1 -f 4x2x1 -s 2x1x0vox -q 30x20x4 -t SyN -m CC -o your_prefix -c 5 -j 8 *T1w.nii.gz

This will run some initial tasks directly on the cluster login node and start
the more work intensive tasks automatically as a SLURM job.

.. note::

   Add an optional ``&`` at the end of the line to run the script in the
   background. Alternatively, you could use :ref:`tmux` to start scripts in the
   background.

We also track a tiny set of patches on top of the regular scripts (as indicated
with the `-mpibX` suffix). One of the changes in these patches reuses the
``-j`` switch to configure the cores used for each of the template building sub
tasks. Tuning this value further improves running time. The table below lists
a simple benchmark for a template building pipeline using the first
9 subjects from the ds000001 dataset using the ``antsMultivariateTemplateConstruction2.sh``
command shown above.

==== ==============
-j X Time (minutes)
==== ==============
4    70
8    39
16   24
32   15
==== ==============

Be careful not to request too many cores per job as you might have to wait too
long for the resources to become available. Using ``-j 8`` should be a good
compromise.

All other ANTs programs need to be wrapped into a job file to be run on the cluster nodes:

.. code-block:: bash

   sbatch -J ants --time 100:0:0 --mem 16gb complex-ants-pipeline.sh


The file ``complex-ants-pipeline.sh`` contains your ANTS commands *preceeded*
by a ``module load ants``. The **walltime** and **memory requirements** given
above are just an example, tune them to your needs.


ANTSr
-----

Hint: If you are more familiar with GNU-R, you might want to checkout some
R-bindings for ANTS: `ANTSr`_.  To install, run this inside your R-session:


.. code-block:: r

   install.packages("drat", repos="http://ftp5.gwdg.de/pub/misc/cran/")
   drat::addRepo("ANTs-R")
   install.packages("ANTsR", repos="http://ftp5.gwdg.de/pub/misc/cran/")

.. _Neurodebian : http://neuro.debian.net/
.. _ANTSr: https://github.com/ANTsX/ANTsR
