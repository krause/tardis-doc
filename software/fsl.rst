FSL
===

We are automatically following the FSL 5 release history of `Neurodebian <http://neuro.debian.net>`_.

To use FSL in your jobs, activate it with environment modules


.. code-block:: bash

   module load fsl


or, manually:

.. code-block:: bash

   source /etc/fsl/5.0/fsl.sh

Afterwards the :file:`PATH` variable should be set correctly to find the FSL programs. You can test this with `which feat` or with `echo $FSL_DIR`.


FEAT
----

Analyzing fMRI data is often done with FSL's feat. Although it's a GUI program,
individual models are defined by a design file and can be started on the
command line with :program:`feat design.fsf` This makes :program:`feat` an
ideal candidate for parallel job submission.

Assume you've got a template :file:`design.fsf` with placeholder values for
different subject ids **%SUBJECT%** and runs **%RUN%**. The following script
will generate individual design files from the template and submit a feat job
for all combinations of subjects and runs in parallel all in one go:


.. code-block:: bash

    for subject in ID1 ID2 ID3; do
        for run in 1 2 3 ; do
          # create a copy of the template with a unique name
          featfile="design.${subject}.${run}.fsf"
          cp design.template $featfile
    
          # replace the placeholder by actual values
          sed -i "s|%SUBJECT%|$subject|g" $featfile
          sed -i "s|%RUN%|$run|g" $featfile
    
          # submit the feat command with the current design file
          sbatch --wrap ". /etc/profile ; module load fsl ; feat $featfile"
        done
      done


FIX
---

We have set up a running version of `FSL Fix <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FIX>`_.
Currently it is not using FSL's internal qsub so it only makes sense to
parallelize over a number of subjects. Example:

.. code-block:: bash

    for icadir in *ica ; do
        echo "#!/bin/bash"                                                            > tmp.sh
        echo "module load fsl_fix"                                                   >> tmp.sh
        echo "source /etc/fsl/5.0/fsl.sh"                                            >> tmp.sh
        echo "fix ${icadir} /opt/software/fix/1.06/training_files/Standard.RData 20" >> tmp.sh
        sbatch -J fix_foobar tmp.sh
    done

The :file:`fsl_fix` module will also load a special R environment module which will
add a library location to R's :file:`.libPaths()` containing necessary packages.
