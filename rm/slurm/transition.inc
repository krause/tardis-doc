.. _slurm_transition:

Torque Transition Table
-----------------------

===================================================   =====
Torque                                                Slurm
===================================================   =====
**Submit**
-----------------------------------------------------------
``qsub job.pbs``                                      ``sbatch job.sh``
``echo "Rscript foo.R" | qsub``                       ``sbatch --wrap "Rscript foo.R"``
``qsub -I -q testing``                                ``srun -p test --pty /bin/bash``
---------------------------------------------------   -----
**Query**
-----------------------------------------------------------
``qstat``                                             ``squeue``
``qstat -r``                                          ``squeue -u $USER --states R``
``qstat -q``                                          ``sinfo -s``
---------------------------------------------------   -----
**Manage**
-----------------------------------------------------------
``qdel 1234``                                         ``scancel 1234``
``qdel all``                                          ``scancel -u $USER``
``qalter -l walltime=24:0:0 1234``                    ``scontrol update jobid=1234 TimeLimit=24:0:0``
===================================================   =====
