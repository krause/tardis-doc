Commands
--------

Submitting
+++++++++++

**Non-Interactively**

Add a single to job the the default queued:

.. code-block:: bash

   sbatch job.slurm

Submit the same job with some resource requests and a name.

.. code-block:: bash

   sbatch --cpus 2 --mem 8G --job-name test job.slurm

Submit a job to the gpu partition, requesting 2 gpus on a single node:

.. code-block:: bash

    sbatch -p gpu --gres gpu:2 job.slurm

Wrap bash commands into a job on the fly:

.. code-block:: bash

    sbatch --wrap ". /etc/profile ; module load R ; Rscript main.R"

**Interactively/Blocking**

Quick interactive, dual-core shell in the test partition:

.. code-block:: bash

    srun -p test -c2 --pty bash

Querying
++++++++

You can use ``squeue`` to get all information about queued or running jobs. This
example limits the output to jobs belonging to the user `krause`:

.. code-block:: bash

    [krause@login ~] squeue  -u krause
                 JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
                110996     short     test   krause  R       0:12      1 ood-43
                110997       gpu job.slur   krause  R       0:08      1 gpu-4
                110995     short job.slur   krause  R       0:15      1 ood-43

As you can see there are 3 jobs, two of them are in the default partition
(**short**) and one has been sent to the gpu partition. They are all in the
running (R) state (ST) and have been running for a couple of seconds (TIME).
**Squeue** is very powerful and its output can be arbiatrarily configured using
format strings.  Checkout ``squeue -o all`` and have a look at the manpage with
``man squeue``.

To look up historical (accounting) data there is ``sacct``. Again, all output columns can be configured. Example:

.. code-block:: bash

    [krause@login ~] sacct -o JobID,ReqMEM,MaxRSS,CPU,Exit
           JobID     ReqMem     MaxRSS    CPUTime ExitCode
    ------------ ---------- ---------- ---------- --------
    110973              4Gc       936K   00:00:08      0:0
    110974              4Gc       936K   00:00:00      0:0
    110976              4Gc       944K   00:00:03      0:0

The ``sacct`` command has many filtering options to help you wade through large
numbers of jobs. Notable examples are ``--starttime``, ``--endtime``, ``--state``, or
``--name`` (does not support wildcards unfortunately, but you can use grep).

Example:
Look up all (historical) jobs for user ``moneta`` on Nikolaustag, that started at
8am or later and finished at 11pm or earlier with the state completed (``CD``).
Limit output to the fields listed with ``-o``. Because output data will be
truncated this way and in this case the job name is longer than 10 characters,
we add a format hint ``%30`` to leave enough room for the complete name.

.. code-block:: bash

    [krause@login~] sacct --starttime 12/06-08:00 --endtime 12/06-23:00 --state CD --parsable --user moneta \
                      -o JobID,JobName%30,TotalCPU,Elapsed,ExitCode | grep MagicM41_3_3_1

    2392298                  MagicM41_3_3_1_1_1             00:35.463   00:00:36      0:0
    2392299                  MagicM41_3_3_1_1_2             00:27.970   00:00:29      0:0
    2392300                  MagicM41_3_3_1_2_1             00:28.047   00:00:29      0:0
    2392301                  MagicM41_3_3_1_2_2             00:26.510   00:00:27      0:0
    2392302                  MagicM41_3_3_1_3_1             00:33.476   00:00:35      0:0
    2392303                  MagicM41_3_3_1_3_2             00:31.711   00:00:33      0:0
    2395291                  MagicM41_3_3_1_1_1             41:48.861   00:41:50      0:0
    2395292                  MagicM41_3_3_1_1_2             54:39.258   00:54:40      0:0
    2395293                  MagicM41_3_3_1_2_1             42:21.278   00:42:23      0:0
    2395294                  MagicM41_3_3_1_2_2             41:47.134   00:41:49      0:0
    2395295                  MagicM41_3_3_1_3_1              01:04:31   01:04:32      0:0
    2395296                  MagicM41_3_3_1_3_2              01:06:34   01:06:36      0:0


If you want to programatically use the data from sacct it's a good idea to use
the ``--parsable`` option. All data will be printed in full and with a pipe (``|``)
symbol as a separator (configurable). Using ``--units``, we can get uniform
scaling of the values.

Example:
Find job ids of jobs with a specific name and other parameters.  In a second
step, query all job "batch" *steps* with that job id for their maximum memory
usage (this not an aggregated value unfortunately), filter values that are
exactly 0 (ended to quickly for sampling) and plot the data in the terminal
using bashplotlib (must be installed with pip).

.. code-block:: bash

    [krause@login~] ids=$(sacct --starttime 12/06-08:00 --endtime 12/06-23:00 --state CD --parsable \
                      --user moneta -o JobID,JobName%30.batch | grep MagicM41 | cut -d"|" -f1)
    [krause@login~] for id in $ids ; do sacct --parsable2 --noheader --units M -o MaxRSS \
                      -j $id.batch; done | tr -d "M" | grep -v "^0$" | hist -b 50 -x

     10|                                       o
      9|                                       o
      8|                                       o
      7|                                      oo
      6|                                 o    oo
      5|                    o     o      o    oo           o
      4|                    o  o  o    o o oo oo        oooo
      3|                   ooo o  o    o o ooooo      oooooo
      2|                   oooooo o  oooooooooooo     oooooo
      1| o                 ooooooooo ooooooooooooo  o oooooo
        ---------------------------------------------------
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
        3 3 3 3 4 4 4 4 4 4 4 5 5 5 5 5 5 6 6 6 6 6 6 6 7 7
        3 5 7 8 0 1 3 4 6 7 9 1 2 4 5 7 8 0 2 3 5 6 8 9 1 3
        . . . . . . . . . . . . . . . . . . . . . . . . . .

    -------------------------
    |        Summary        |
    -------------------------
    |   observations: 100   |
    | min value: 133.880000 |
    |   mean : 160.114600   |
    | max value: 173.080000 |
    -------------------------


For more options and output fields, have a look at the wonderful manual page
(``man sacct``).


Deleting
++++++++

You can cancel a specific job by running ``scancel JOBID`` or all of
your jobs at once  with ``scancel -u $USER``. This is a bit
different to Torque as there is no special **all** placeholder.
Instead you just ask the system to cancel jobs matching to your
username. Of course it's not possible to accidentally cancel other
user's jobs.
