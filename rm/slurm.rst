SLURM
=====


`SLURM`_ is the resource manager that replaced Torque on the Tardis in 2020. It
is similar to Torque in its main concepts, but the commands and syntax differs
a little. We switched, because it is much more flexible than Torque, actively
maintained, and supports sophisticated GPU scheduling. We will gradually move
nodes from Torque to SLURM to motivate everyone to familiarize with the new system.

**Users familiar with Torque** can check out the :ref:`slurm_transition` for a quick start:


.. include:: slurm/transition.inc
.. include:: slurm/jobs.inc
.. include:: slurm/commands.inc
.. include:: slurm/resources.inc
.. include:: slurm/gpus.inc

.. _SLURM: https://slurm.schedmd.com/
