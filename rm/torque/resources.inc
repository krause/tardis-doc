.. _torque_resources:

Resources and Options
---------------------

Here is a list of common pbs options. You can either use these options directly
with ``qsub`` or add them as meta-parameters in a job file. In the later case
those options need the prefix ``#PBS`` and must be stated in the first section
of the file before the actual commands. The complete list can be found in ``man
pbs_resources``.

``#PBS -N job-name``
   Sets the name of the job. This is mostly useful when submitting lot's of
   similar jobs in a loop.

``#PBS -l *nodes=1:ppn=8*``
    Request a single node with *8 physical cores*. The default will always be
    just a single core. Use this option when your job is multi-threaded (OpenMP
    for example). If you intend to run multi-node jobs (MPI-enabled programs)
    you can request multiple machines with this option as well.

``#PBS -l walltime=100:0:0``
    Sets the expected maximum running time for the job. When a job **exceeds**
    those limits it will **be terminated**.

``#PBS -l mem=10gb``
    Sets another resource requirement: memory. Exceeding this value in a job is
    even more crucial than running time as you might interfere with other jobs
    on the node. Therefor it needs to be **terminated as well**.

``#PBS -d project/data``
    Sets the working directory of the job. Every time a job gets started it
    will spawn a shell on some node. To initially jump to some directory use
    this option. *Otherwise* the first command of your job should always be ``cd
    project/data``.


``#PBS -o /home/mpib/krause/logs/``
    Specify the location where Torque will save the jobs' log file. The
    *stdout* and *stderr* streams will be saved into a file called
    ``<jobname>.o<jobid>`` and ``<jobname>.e<jobid>`` in the current location
    per default. If the parameter to ``-o`` is a file then it will store the
    log in that file. If it's a directory it will put the logs into that
    directory with the default naming scheme.

``#PBS -o /dev/null``
    To discard all standard output log use the special file ``/dev/null``.

``#PBS -e /dev/null``
    The same is true for the standard error log stream.

``#PBS -j oe``
    This option will tell torque to merge stderr into stdout and only store
    a single file.

``#PBS -j eo``
    This will merge stdout into stderr instead.

``#PBS -M krause[,knope,dwyer]``
    Send an e-mail to a single user or a list of users in case there is
    a configured event.

``#PBS -m <str>``
    This paramter defines e-mail events. You can use a combination of **a,b,e**
    or the single value **n**:

    + **a** defines external job termination as an e-mail event
    + **b** defines the beginning of a job run as event
    + **e** send an e-mail when the job has finished

``#PBS -W depend=afterok:Job-Id``
    This will add a dependency to the current job. It will only be started or
    tagged as startable when another job with id *Job-Id* finished
    successfully.

``#PBS -h``
    Set the job in a holding state. This will prevent it from being started.
    Release the hold with ``qrls <jobid>``.
