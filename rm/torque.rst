.. _torque_index:

Torque **(End-Of-Life)**
========================

`Torque <http://www.adaptivecomputing.com/products/open-source/torque>`_ is a **PBS**-based resource manager, originally developed by NASA and
currently maintained by Adaptive Computing Inc. A necessary, but problematic
requirement for Torque is the independent scheduler. For a long time we used
the open source product Maui, which hadn't seen an update for many years and
became incompatible to some of the more recent feature of Torque.


.. include:: torque/jobs.inc
.. include:: torque/commands.inc
.. include:: torque/dependencies.inc
.. include:: torque/resources.inc
