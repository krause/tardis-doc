#!/bin/bash
set -e

SRC=$1
DST=$2

cd $1
make html
cd -
mkdir -p $2
cp -r $1/_build/html/. $2
exit 0
