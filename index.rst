Welcome to Tardis's documentation!
==================================

Changelog
=========

3.9.6 (31.01.2023)
  + add some documentation for Mountain Duck

3.9.5 (20.02.2023)
  + clarify current recommended use of venv of virtualenv(wrapper) in python

3.9.4 (25.01.2023)
  + singularity is now apptainer, updated the documentation and examples accordingly

3.9.3 (20.01.2023)
  + added examples for CAT12 using containerized standalone scripts

3.9.2 (17.01.2022)
  + added examples and benchmarks for QSIprep (in BIDSApps)

3.9.1 (09.12.2021)
  + compiling Matlab is usually not necessary anymore

3.9.0 (09.12.2021)
  + add some notes about BIDSApps

3.8.0 (16.07.2020)
  + Torque is being deprecated
  + added documentation for the new rm SLURM
  + examples of how to use GPUs

3.7.0 (03.04.2020)
  + introduce the /data folder
  + added section about singularity
  + finally documented environment modules

3.6.2 (17.12.2019)
  + new memory defaults for compiled spm12

3.6.1 (19.09.2019)
  + added minimal howto for Conda
  + updated SPM12 section (env modules)

3.6 (02.07.2019)
  + added section about Torque Dependencies

3.5 (07.07.2018)
  + added section about (non-)efficient saving of objects in Matlab

3.4.5 (09.02.2018)
  + major changes with ASHS

3.4.4 (19.12. 2017)
  + updated Tardis specs
  + added FSL feat example
  + added new section for useful \*nix programs (tmux)

3.4.3 (22.12. 2016)
  + updated ashs chunk submit script

3.4.2 (11.11. 2016)
  + added python section

3.4.1 (18.10. 2016)
  + added fieldtrip compilation notes

3.4 (19.09. 2016)
  + added matlab section

3.3 (15.08. 2016)
  + added minimal example for fsl fix
  + fixed a typo with the `qdel` example

3.2 (18.07. 2016)
  + restructuring and some typos (thanks for the feedback!)

3.1 (01.07 2016)
  + added documentation for Freesurfer and ASHS
  + fixed some typos

3.0 (24.10 2015)
  + We switched to the `Sphinx <http://sphinx-doc.org/>`_ documentation system!


.. toctree::
   :maxdepth: 1
   :caption: General
   :hidden:

   introduction/index
   introduction/login
   introduction/rules
   introduction/data

.. toctree::
   :maxdepth: 3
   :caption: Resource Manager
   :hidden:

   rm/general
   rm/slurm
   rm/torque

.. toctree::
   :maxdepth: 1
   :caption: Environment
   :hidden:

   environment/modules
   environment/apptainer
   environment/unix

.. toctree::
   :maxdepth: 1
   :caption: Software
   :hidden:

   software/ants
   software/ashs
   software/BIDSApps
   software/freesurfer
   software/fsl
   software/matlab
   software/python
   software/r

.. toctree::
   :maxdepth: 1
   :caption: Other
   :hidden:

   other/faq
